const { users } = require("../models");

class usersRepository {
    static async getByID({ id }) {
      const getUser = await users.findOne({ where: { id } });
  
      return getUser;
    }
  
    static async getByEmail({ email }) {
      const getUser = await users.findOne({ where: { email } });
  
      return getUser;
    }

  static async register({ name, email, password, role }) {
    const registered_Users = users.create({
      name,
      email,
      password,
      role,
    });

    return registered_Users;
  }
}

module.exports = usersRepository;
